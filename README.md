# Combine Container

This image contains compiled versions of HiggsCombine and CombineHarvester.

Current versions:

- CMSSW: CMSSW_11_3_4
- HiggsCombine: v9.2.1
- CombineHarvester: v2.1.0

## Use with Apptainer and CVMFS (e.g. LXPLUS)

```shell
export APPTAINER_CACHEDIR="/tmp/$(whoami)/apptainer_cache"
apptainer shell -B /cvmfs -B /eos -B /afs /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-analysis/general/combine-container:CMSSW_11_3_4-combine_v9.2.1-harvester_v2.1.0 /bin/bash
```

Then in the Apptainer shell:

```shell
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr/CMSSW_11_3_4/
cmsenv  # Ignore errors
```

Then scripts such as `text2workspace.py` should be available and can be used with files on EOS and AFS.

## Use in CI

```yaml
test_cards:
  tags:
    - cvmfs
  image: gitlab-registry.cern.ch/cms-analysis/general/combine-container:CMSSW_11_3_4-combine_v9.2.1-harvester_v2.1.0
  before_script:
    - source /cvmfs/cms.cern.ch/cmsset_default.sh
    - cd /home/cmsusr/CMSSW_11_3_4/
    - cmsenv || true
  script:
    - text2workspace.py --help
```
