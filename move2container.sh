#!/bin/bash

# exit when any command fails; be verbose
set -ex

# make cmsrel etc. work
shopt -s expand_aliases
export MY_BUILD_DIR=${PWD}
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/cmsusr
rsync -auvq ${MY_BUILD_DIR}/CMSSW_* .
cd CMSSW_*/src
scram b ProjectRename
cmsenv
scram b
